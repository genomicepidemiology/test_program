import pytest
import sys
import subprocess
import pandas as pd
import os
import tarfile
import shutil


## Dictionary including the local paths for the performing of the test and service ##

PATHS={
'CONFIG_FILE':'/home/alfred/Projects/test_program/test_results/config_file',
'BLASTN':'/home/alfred/bio_tools/ncbi-blast-2.8.1+/bin/blastn',
'KMA':'/home/alfred/bio_tools/kma/kma',
'VIRULENCE_SERVICE':'/home/alfred/Projects/test_program/virulencefinder/virulencefinder.py',
'DATABASE':'/home/alfred/Projects/test_program/virulencefinder/virulencefinder_db/',
'INPUT_DATA':'/home/alfred/Projects/test_program/test_input_files/',
'EXPECTED_RESULTS':'/home/alfred/Projects/test_program/test_results/'
}

## Read the config file ##
### TODO: Option for not reading all the files in config file ###

config_file = pd.read_csv(PATHS['CONFIG_FILE'],sep="\t",comment="#")



class read_test_files:

    """Class for containing the functions to read the different files that the
    service produces"""

    def tsv_files(file):

        """Simple function for reading a tsv file"""

        file=pd.read_csv(file,sep="\t")
        return file

class run_service:

    """Class for running the service based on the config file to produce the
    results in the temporary directory, and move the expected results to the
    same run temporary directory."""

    def __init__(self, config_data):

        """Define the variables for running the service based on the config
        file"""

        self.config_data = config_data
        self.file = config_data['file']
        self.input_file = config_data['input_file']
        self.name = config_data['name']
        self.database = config_data['database']
        self.coverage = config_data['coverage']
        self.identity = config_data['identity']
        self.extended_output = config_data['extended_output']
        if config_data['data_type'] == 'Assembled':
            self.method = PATHS['BLASTN']
        else:
            self.method = PATHS['KMA']

    def run_virulencefinder(self):

        """Run the service"""

        if self.extended_output:
            output_variable=" -x"
        else:
            output_variable=""

        if self.database=="All":
            cmd=("python3 "+PATHS['VIRULENCE_SERVICE']+" -i "+
            PATHS['INPUT_DATA']+str(self.input_file)+" -o "+str(self.dir_out)+
            " -p "+PATHS['DATABASE']+" -mp "+str(self.method)+" -l "+
            str(float(self.coverage)/100.)+" -t "+str(float(self.identity)/100.)
            +" -q"+str(output_variable))
        else:
            cmd=("python3 "+PATHS['VIRULENCE_SERVICE']+" -i "+
            PATHS['INPUT_DATA']+str(self.input_file)+" -o "+str(self.dir_out)+
            " -p "+PATHS['DATABASE']+" -d "+str(self.database)+" -mp "+
            str(self.method)+" -l "+str(float(self.coverage)/100.)+" -t "+
            str(float(self.identity)/100.)+" -q"+str(output_variable))
        subprocess.run(cmd,shell=True)

    def create_outputrun(self,tmpdir):

        """Force the service to run in the temporary directory and copy the
        expected results in the temporary directory"""

        #Create in the temporary-run folder the folders: output_"name", where the
        #output of the service is saved; file_test, where the expected results are saved
        dir_out = tmpdir.mkdir("outputs_"+str(self.name))
        self.dir_out=dir_out
        dir_files = tmpdir.mkdir("file_test")
        shutil.copy2(PATHS['EXPECTED_RESULTS']+str(self.file),str(tmpdir)+
            "/file_test/"+str(self.file))
        self.run_virulencefinder()

def create_parameters(config):

    """Iterate over the files listed in the config file to create the fixture
    for pytest"""

    parameters_list=[]
    for index, rows in config.iterrows():
        parameters_list.append(rows)
    return parameters_list

parameters_list=create_parameters(config_file)


## Use the fixture for running pytest
@pytest.mark.parametrize("data_config",parameters_list)
def test_check_equal(tmpdir,data_config):

    """Run the service and move the data to the temporary directory. Compare the
    output of the service with the expected results, comparing the
    results_tab.tsv file"""

    r=run_service(data_config)  #Declare the class
    r.create_outputrun(tmpdir)  #Run the service in the temporary directory
    #Read output of the service
    output_file=read_test_files.tsv_files(str(tmpdir)+"/outputs_"+
        str(data_config['name'])+"/results_tab.tsv")
    #Open the .tar.gz file and find the results_tab.tsv file of the expected results
    tar_test=tarfile.open(str(tmpdir)+"/file_test/"+str(data_config['file']),mode="r:gz")
    for member in tar_test.getmembers():
        if member.name==str(data_config['name'])+"/results_tab.tsv":
            tab_file=tar_test.extractfile(member)
            test_file=read_test_files.tsv_files(tab_file)
    #Compare both .tsv files
    assert test_file.equals(output_file)
