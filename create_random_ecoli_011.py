#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import os
import re
from Bio.Seq import Seq
from Bio import SeqIO
from random import sample
import json
import datetime
from datetime import date
from datetime import datetime
from datetime import time
import pandas as pd
from pandas import DataFrame
import numpy as np
import tarfile
from tabulate import tabulate
import shutil


# Save current working directory
user_path = os.getcwd()


def text_table(headers, rows, empty_replace='-'):
    '''
    OBS: THIS FUNCTION IS COPIED DIRECTLY FROM CGE VIRULENCEFINDER
    Create text table

    USAGE:
        >>> from tabulate import tabulate
        >>> headers = ['A','B']
        >>> rows = [[1,2],[3,4]]
        >>> print(text_table(headers, rows))
        **********
          A     B
        **********
          1     2
          3     4
        ==========
    
    '''
    
    # Replace empty cells with placeholder
    rows = map(lambda row: map(lambda x: x if x else empty_replace, row), rows)
    
    # Create table
    table = tabulate(rows, headers, tablefmt='simple').split('\n')
    
    # Prepare title injection
    width = len(table[0])
    
    # Switch horisontal line
    table[1] = '*' * (width + 2)
    
    # Update table with title
    table = (("%s\n" * 3)
             % ('*' * (width + 2), '\n'.join(table), '=' * (width + 2)))
    return table


####### CREATION OF TEST FILES ########
class create_files:
    
    def __init__(self, database):
        """Init function """
        self.database = database
        # Match anything but the last dot - thereby matching only the filename without the file ending
        self.database_name = re.search(r'.*(?=\.)',self.database).group(0)
        
        # Dict for input filenames and their sequences
        self.sequences_dict = dict()
        

    ### Functions ###
    # 1 - 1 - 1 - 1 - 1 - 1 - 1 #
    def obtain_random_virulence_genes(self,number_virulence,overwrite,select_random,format,sequences_in_each_file, whole_genom, max_length_genom):
        
        """Function that extracts sequences of virulence genes from database and prints them into seperate files.
        number_virulence is the number of virulence genes, that will be extracted.
        overwrite can be either 'o' or another string. If it's 'o', any existing files created by prior runs of the program will be overwritten.
        select_random can be either 'r' or another string. If it's 'r', the program will draw random entries from the database. If it's anything else,
        the program will pick the first entries from the database.
        Format decides the format of the test file - fasta or fastq.
        sequences_in_each file (integer) decides how many sequences is printed into each test file. If input is 'r', a random number of sequences will be printed in
        each file.
        whole_genom can be 'whole' or any other string. If it's 'whole', the function will create random genoms around the drawn virulence genes and print these whole genoms
        into files instead of just the virulence genes. 
        max_length_genom (integer) will be the maximum number of random bases in the whole genoms created around the virulence genes - if whole_genom is chosen.
        Can be anything if whole_genom 'version' is not chosen."""

        os.chdir(user_path)
        
        # Save format for use in later functions
        self.format = format
        
        # Delete directories with output files if the user has chosen to overwrite them
        if os.path.exists('test_input_files') and overwrite == 'o':
            shutil.rmtree('test_input_files', ignore_errors=True)
        
        # Make directory if it doesn't exist
        if not os.path.exists('test_input_files'): 
            os.makedirs('test_input_files')
        
        # Change directory to test_input_files
        os.chdir('test_input_files')
        
        # Check if there are already files present in the directory:
        content_list = os.listdir(os.getcwd())
    
        # Sort them alphabetically
        content_list.sort()
        
        # If there are no files in the input directory
        if content_list ==[]:
            # First file index is one
            indexnumber = 1

        # Otherwise just add 1 to index number to the latest index number
        else:
            # Find the index number
            number_in_last_file = re.search(r'[0-9]+',content_list[-1]).group(0)
            # Add one
            indexnumber = int(number_in_last_file) + 1
        
        # Go to original path
        os.chdir(user_path)
        
        # Extract the selected number of random entries (using Biopython)
        if select_random == 'r':
            entries_list = sample(list(SeqIO.parse(self.database, "fasta")),number_virulence)
            
        else:
            entries_list = list(SeqIO.parse(self.database, "fasta"))[0:number_virulence]
        
        # Create whole genom if chosen
        if whole_genom == 'whole':
            # Add bases to create a fake genom
            posible_bases = ['A','T','C','G']
            
            for entry in entries_list:
                # Draw random number (length of the genom)
                length = random.randint(0,max_length_genom)
            
                # Create random genom with the drawn length
                genom = random.choices(posible_bases, k = length)
                genom = ''.join(genom)
                
                # Find random position and insert virulence gene
                pos = random.randint(0,length)
                
                genom_with_vg = str(genom[:pos] + str(entry.seq) + genom[pos:])
                
                # Save position where virulence gene begins and ends to seq record for later use
                entry.start = pos
                entry.end = pos + len(entry.seq)
                
                # Add old sequence as element in seq record for later use
                entry.old_seq = entry.seq
    
                # Overwrite seq-element in entry with whole genom
                entry.seq = Seq(genom_with_vg) # NOTE: No alphabet specified because the Bio.Alphabet will be removed in 2020
        
        # General file name
        name = 'test_virulence_ecoli_gene'
        
        # Create dictionary with filenames as keys (exclusive the end) and list of SeqRecords as values.
        if sequences_in_each_file == 'r':
            number_of_sequences = random.randint(1,len(entries_list))
        else:
            number_of_sequences = sequences_in_each_file
        
        # Assign the wanted number of sequences to files for as long as possible; assign remaining sequences to last file 
        remaining = len(entries_list)
        pos = 0
        
        while remaining >= number_of_sequences:
            input_filename = str(name + str(indexnumber))
            
            self.sequences_dict[input_filename] = entries_list[pos:pos+number_of_sequences]
        
            remaining = remaining - number_of_sequences
            
            pos += number_of_sequences
            
            indexnumber += 1
            
        if pos < len(entries_list):
            input_filename = str(name + str(indexnumber))
            
            self.sequences_dict[input_filename] = entries_list[pos:]
        
        
        # Create either fasta or fastq files with the obtained sequences
        os.chdir('test_input_files')
        
        # FASTA
        if self.format == 'fasta':
            for filename in self.sequences_dict.keys():
                outfile = open(str(os.getcwd() + '/' + filename + '.fsa'),'w')
                
                # Write the random sequences with header in the file (using Biopython)
                for entry in self.sequences_dict[filename]:
                    SeqIO.write(entry,outfile,"fasta")
                    
                outfile.close()
        
        # FASTQ
        elif self.format == 'fastq':
            
            # Loop that creates fastq files with the obtained sequences
            for filename in self.sequences_dict.keys():
                outfile = open(str(os.getcwd() + '/' + filename + '.fastq'),'w')
                
                for entry in self.sequences_dict[filename]:
                    # Reset quality sequence
                    quality_seq = ''
                    
                    # Write first three lines
                    outfile.write('@' + entry.id +'\n')
                    outfile.write(str(entry.seq)+'\n')
                    outfile.write('+ \n')
                    
                    # Create random quality sequence
                    for i in range(len(entry.seq)):
                        
                        # Draw random quality number within the right interval
                        number = random.randint(52, 72)
                        
                        # Change drawn number to ASCII character
                        quality_seq += chr(number)
                    
                    # Write quality sequence to fastq file
                    outfile.write(quality_seq + '\n')

                # Update file number
                indexnumber += 1
                
                outfile.close()
                
                
    def create_results_folders(self, overwrite):
        """ Function that creates the required directories for outputs.
        overwrite can be 'o' or anything else. If it is 'o', any existing directory named test_results will be deleted and a new one will be created containing only the
        results of the files created within this run of the program. If 'o' is not chosen, any old directories within test_results with other names than those created
        in this run of the program, will be saved."""

        os.chdir(user_path)
        
        # Delete directories with result files if the user has chosen to overwrite them
        if os.path.exists('test_results') and overwrite == 'o':
            shutil.rmtree('test_results', ignore_errors=True)
        
        # Make directory if it doesn't exist and go to directory
        if not os.path.exists('test_results'): 
            os.makedirs('test_results')  
        os.chdir('test_results')
        
        # Create result folders for each input file
        for filename in self.sequences_dict.keys():
            # Find number in filename
            number = re.search(r'[0-9]+', filename).group(0)
            folder_name = str('test_ecoli' + str(number))
            
            # Delete folders with same name that already exists (because the number in the filename shows whether the user has chosen to overwrite the old test input files or not)
            if os.path.exists(folder_name):
                shutil.rmtree(folder_name, ignore_errors=True)
            
            # Create results file directory
            os.makedirs('test_ecoli'+str(number))
    
    
    def write_results_tsv(self):
        """Function that creates a tsv file with the results. To be run after creating input files via obtain_random_virulence_genes function and output directories
        via create_results_folders."""
        
        os.chdir(user_path + '/test_results')

        # Extract the data within input files
        for filename in self.sequences_dict.keys():
            # Go to correct results folder
            filename_index = int(re.search(r'[0-9]+',filename).group(0))
            os.chdir(user_path + '/test_results/test_ecoli' + str(filename_index))
            
            # Dataframe for the tsv file:
            results= {
                'Database': [],
                'Virulence Factor': [],
                'Identity' : [],
                'Query / Template length' : [],
                'Contig' : [],
                'Position in contig' : [],
                'Protein function': [],
                'Accession number': []
                }
            
            # Extract information for each entry in the input file
            for entry in self.sequences_dict[filename]:
                ID = entry.id
                contig_list = entry.id.split(":")
                virulence_factor = contig_list[0]
                accession_number = contig_list[2]
                database = re.search(r'(\w+)$',self.database_name).group(0)
                
                if 'old_seq' in dir(entry):
                    seq_length = len(entry.old_seq)
                    virulence_start = entry.start
                    virulence_end = entry.end
                else:
                    seq_length = len(entry.seq)
                    virulence_start = 0
                    virulence_end = seq_length
                    
                template_length = seq_length # because of 100% coverage
                
                # Add to dict
                results['Database'] += [database]
                results['Virulence Factor'] += [virulence_factor]
                results['Identity'] += [100.0] 
                results['Query / Template length'] += [str(seq_length)+' / '+str(template_length)] 
                results['Contig'] += [ID]
                results['Position in contig'] += [str(virulence_start) + '..' + str(virulence_end)] 
                results['Protein function'] += ['Unknown']
                results['Accession number'] += [accession_number]
        
                # Create dataframe and export to tsv
                df = DataFrame.from_dict(results)
                tsv_export = df.to_csv (user_path + '/test_results/test_ecoli' + str(filename_index) + '/results_tab.tsv', index = None, sep = '\t', header = True)


    def write_json(self):
        """ Function that creates the json result file(s).To be run after creating input files via obtain_random_virulence_genes function and output directories
        via create_results_folders."""
        
        os.chdir(user_path)
        
        # Define date and time - and format to correct order 
        current_date = date.today()
        current_date_format = current_date.strftime('%d.%m.%Y')
        current_time = datetime.now()
        current_time_format = current_time.strftime('%H:%M:%S')
        
        # Loop that creates a json file for every input file created
        for filename in self.sequences_dict.keys():
            organism = 'Escherichia coli'
            indexnumber = int(re.search(r'[0-9]+',filename).group(0))
            
            outfile = open(str(user_path + '/test_results/test_ecoli' + str(indexnumber) +'/data.json'),'w')
            
            output_dict = {
                "virulencefinder": {
                    "user_input": {
                        "filename(s)": [str(user_path + '/test_input_files')],
                        "method": float('nan'), 
                        "file_format": self.format},
                    "run_info": {
                        "date": current_date_format,
                        "time": current_time_format},
                    "results":{
                        "Escherichia coli":{ 
                            "virulence_ecoli": { 
                            }}}}}
            
            # Add values to output dict
            for entry in self.sequences_dict[filename]:
                id_list = entry.id.split(":")
                
                output_dict["virulencefinder"]["results"]["Escherichia coli"]["virulence_ecoli"][str(entry.id)] ={
                    "virulence_gene": id_list[0],
                    "identity": float(100.0), 
                    "HSP_length": float('nan'),
                    "template_length": len(entry.seq), 
                    "position_in_ref": float('nan'),
                    "contig_name": entry.id,
                    "positions_in_contig": float('nan'), 
                    "note": id_list[1],
                    "accession": id_list[2],
                    "protein_function": float('nan'), 
                    "coverage": float(100.0),
                    "hit_id": float('nan')}
            
            # Print to json file
            output_json = json.dumps(output_dict)
            print(output_json, file = outfile)
            outfile.close()
            

    def write_hitfile(self):
        """ Function that creates the hitfile(s).To be run after creating input files via obtain_random_virulence_genes function and output directories
        via create_results_folders."""
        
        os.chdir(user_path)
        
        # Loop that creates a result file for every created input file
        for filename in self.sequences_dict.keys():
            # Open correct output file
            indexnumber = int(re.search(r'[0-9]+',filename).group(0))
            outfile = open(str(user_path + '/test_results/test_ecoli' + str(indexnumber) +'/Hit_in_genome_seq.fsa'),'w')
            
            for entry in self.sequences_dict[filename]:
                # Create header in hitfile format
                id_list = entry.id.split(":")
                header = '>' + id_list[0] + ': ID:100.0% COV:100.0% Best_match:' + ':'.join(id_list) + '\n'
                
                # Write to file in fasta format
                outfile.write(header)
                
                if 'old_seq' in dir(entry):
                    sequence = str(entry.old_seq)
                else:
                    sequence = str(entry.seq)
                
                for i in range(0,len(sequence),60):
                    print(sequence[i:i+60],file = outfile)
                
            
    def write_virulence_genes_file(self):
        """ Function that creates the virulence gene file(s).To be run after creating input files via obtain_random_virulence_genes function and output directories
        via create_results_folders."""
        
        # Create virulence gene file for every created input file
        for filename in self.sequences_dict.keys():
            os.chdir(user_path)
            indexnumber = re.search(r'[0-9]+',filename).group(0)
            outfile = open(str(user_path + '/test_results/test_ecoli' + str(indexnumber) +'/Virulence_genes.fsa'),'w')
            
            for entry in self.sequences_dict[filename]:
                if 'old_seq' in dir(entry):
                    outfile.write('>' + entry.id + '\n')
                    for i in range(0,len(entry.old_seq),60):
                        print(str(entry.old_seq)[i:i+60],file = outfile)
                else:
                    SeqIO.write(entry,outfile,'fasta')
                    
    
    def write_results_txt(self):
        """ Function that creates the result text file(s).To be run after creating input files via obtain_random_virulence_genes function and output directories
        via create_results_folders."""
        
        for filename in self.sequences_dict.keys():
            os.chdir(user_path)
            
            # Create output file 
            organism = 'Escherichia coli'
            indexnumber = re.search(r'[0-9]+',filename).group(0)
            outfile = open(str(user_path + '/test_results/test_ecoli' + str(indexnumber) +'/results.txt'),'w')
            
            outfile.write("virulencefinder Results\n\n")
            outfile.write("Organism(s): " + str(organism)+'\n\n')
            outfile.write("Virulence genes for Escherichia coli\n")
            
            vir_gene = ""
            identity = ""
            template_HSP = ""
            contig_name = ""
            position_in_contig = ""
            protein_function = ""
            acc = ""
            
            db_rows = []
            
            # Add rows to result tables, create header and write table to outfile
            db_rows.append([vir_gene, identity, template_HSP, contig_name, position_in_contig, protein_function, acc])
            
            header = ["Virulence factor", "Identity", "Query / Template length", "Contig",
            "Position in contig", "Protein function", "Accession number"]
            
            outfile.write(text_table(header, db_rows) + "\n")
            
            outfile.write("\n\nExtended Output:\n\n")
            
            # Write extended output for every entry in the input file
            for entry in self.sequences_dict[filename]:
                outfile.write("# " + str(entry.id) + "\n")
                
                # Save ID
                query_id= entry.id
                
                # Save sequence obtained from database
                if 'old_seq' in dir(entry):
                    sbjct_seq = str(entry.old_seq)
                else:
                    sbjct_seq = str(entry.seq)
                
                # Save sequence in file (right now same as above, but this should be changed if any mutations are introduced into the sequences in the files)
                if 'old_seq' in dir(entry):
                    query_seq = str(entry.old_seq)
                else:
                    query_seq = str(entry.seq)
                
                # Make 'allignment' string of | with the same length as the sequence from the file (right now only | because the sequences in the file are taken directly
                # from the database. As above, this should be changed if any mutations are introduced into the sequences in the files)
                homol_seq = len(sbjct_seq) * '|'
                
                # Write to file
                for i in range(0, len(sbjct_seq), 60):
                    outfile.write("%-10s\t%s\n" % ("template:", sbjct_seq[i:i + 60]))
                    outfile.write("%-10s\t%s\n" % ("", homol_seq[i:i + 60]))
                    outfile.write("%-10s\t%s\n\n" % ("query:", query_seq[i:i + 60]))


    def write_config(self):
        """ Function that creates the config file(s).To be run after creating input files via obtain_random_virulence_genes function and output directories
        via create_results_folders."""
        
        # Change directory to test_results
        os.chdir(user_path + '/test_results')
        
        # Define values
        coverage = 100
        identity = 100
        
        if self.format == 'fasta':
            data_type = 'Assembled'
        elif self.format == 'fastq':
            data_type = "Raw"
        
        # Config dict
        config = {
            'file': [],
            'name': [],
            'input_file' : [],
            'extended_output' : [],
            'database' : [],
            'coverage' : [],
            'identity': [],
            'data_type': []
            }
        
        # Add information about all files to dict
        for filename in self.sequences_dict.keys():
            
            config['file'] += [str(filename) + '.tar.gz']
            config['name'] += [filename]
            config['input_file'] += [self.database]
            config['extended_output'] += ['True'] 
            config['database'] += [self.database_name]
            config['coverage'] += [str(coverage)]
            config['identity'] += [str(identity)]
            config['data_type'] += [data_type]
        
        # Convert dict to dataframe and export to tsv
        df = DataFrame.from_dict(config)
        comment_header = pd.DataFrame({"#Config file describing as an index what is the content of each compressed folder\n #Each .tar.gz contains: Virulence_genes.fsa, results.txt, tmp/out_virulence_ecoli.xml, Hit_in_genome_seq.fsa, data.json, results_tab.tsv"})
        comment_header.to_csv('config_file', index=False, header=None)
        df.to_csv ('config_file', index = None, sep = '\t', header = True, mode='a')


    def zip_function(self):
        """Function that zips all the files in cwd and removes the files so they only occur in the zip archive. Should be run last."""
            
        # Change directory to test_results and list the content
        os.chdir(user_path + '/test_results')
        content_list = os.listdir(os.getcwd())
        
        # Zip directory and delete afterwards
        for directory in content_list:
            if directory == 'config_file':
                pass
            else:
                # Additional test to make sure we are only removing folders starting with 'test'
                if directory.startswith('test'):
                    tar = tarfile.open(directory + '.tar.gz' , "w:gz")
                    tar.add(directory)
                    tar.close()              
                    shutil.rmtree(directory)


r = create_files('/Users/sofietheisenhonore/Dropbox/CGE/Virulence_finder/virulencefinder_db/virulence_ecoli.fsa')
r.obtain_random_virulence_genes(4,'o','r','fastq',2,'nwhole',100)
r.create_results_folders('o')
r.write_results_tsv()
r.write_json()
r.write_hitfile()
r.write_virulence_genes_file()
r.write_results_txt()

r.write_config()
r.zip_function()
