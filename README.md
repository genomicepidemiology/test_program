# Test_program

This project contains a first attempt to create tests for the services of CGE.


## Documentation


The Test_program repository contains the python script test_virulence.py, based on the pytest framework,
that test the service VirulenceFinder. The repository also contains folders to store the data necessary, 
and the config file (inside the test_results folder) describing the parameters to recreate the expected
results.

## Content of the repository

1. **test_virulencefinedr.py**	-	the pytest-based program
2. **test_input_files**	-	folder for containing the biological sequence data used for the tests. Includes a simple example
3. **test_results**	-	folder containing the expected results, and the config file describing the variables to create those results. Includes simple examples
4. **README**

## Installation
### VirulenceFinder
Setting up VirulenceFinder program
```bash
# Go to wanted location for virulencefinder
cd /path/to/some/dir
# Clone and enter the virulencefinder directory
git clone https://bitbucket.org/genomicepidemiology/virulencefinder.git
cd virulencefinder
```

#### Download and install VirulenceFinder database

```bash
# Go to the directory where you want to store the virulencefinder database
cd /path/to/some/dir
# Clone database from git repository (develop branch)
git clone https://bitbucket.org/genomicepidemiology/virulencefinder_db.git
cd virulencefinder_db
VIRULENCE_DB=$(pwd)
# Install VirulenceFinder database with executable kma_index program
python3 INSTALL.py kma_index
```

If kma_index has no bin install please install kma_index from the kma repository:
https://bitbucket.org/genomicepidemiology/kma.

It also requires to have installed blastn.

The python packages required are:
`argparse, tabulate, cgecore, distutils, sys, os, time, re, subprocess, json, gzip, pprint`

### Test_program

Setting up Test_program
```bash
# Go to wanted location for Test_program
cd /path/to/some/dir
# Clone and enter the test_program directory
git clone https://bitbucket.org/genomicepidemiology/test_program.git
cd test_program
```

The python packages required are:
`pytest, sys, subprocess, pandas, os, tarfile, shutil`

To make it work locally, it has to be changed the python dictionary "PATHS" written in the lines 9-17 of
the test_virulence.py file to your local paths.

## Usage
### Config file and expected data

The expected data has to be contained in a compressed folder (folder.tar.gz)
The config file has the columns:

1. **file**	-	Name of the folder of the expected results compressed
2. **name**	-	Name of the folder of the expected results
3. **input_file**	-	Name of the sequence data used as input for the service. 
If the input data is two fastq files, the name will contain a * in the letter/number not equal between files.
4. **extended_output**	-	True/False. If the expected result is from a extended_output (-x in virulencefinder.py)
5. **database**	-	The name to set a specific database, or "All" if it not a specific database wants to be setted.
6. **coverage**	-	Set threshold for minimum % coverage
7. **identity**	-	Set threshold for mininum blast % identity
8. **data type**	-	"Assembled" if the input data is FASTA (Assembled or Draft Genomes/Contigs; "Raw" if the input data is FASTQ (Raw Sequencing Data)

### Example of usage

After having gone through all the previous steps,
```bash
pytest test_virulence.py
```

